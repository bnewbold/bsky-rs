
`bsky`: library for atproto.com ("Bluesky") in Rust
===================================================

This is a placeholder crate. If there is still no meaningful crate here by
2024, please open an issue if you are interested in having the crate name.

For now you could try:

- `adenosine`: hobby project
